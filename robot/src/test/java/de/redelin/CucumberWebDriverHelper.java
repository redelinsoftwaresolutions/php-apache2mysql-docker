package de.redelin;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class CucumberWebDriverHelper {

  private static String OS = System.getProperty("os.name").toLowerCase();
  private Properties seleniumProperties;
  private WebDriver wd;

  /**
   * public default constructor supporting dependency injection
   * 
   * @throws FileNotFoundException
   * @throws IOException
   */
  public CucumberWebDriverHelper() {
    try {
      this.seleniumProperties = loadProperties();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * get the initialized WebDriver
   * 
   * @return instance of WebDriver
   */
  public WebDriver getWebDriver() {
    if (wd == null) {
      try {
        this.wd = initWebDriver();
      } catch (MalformedURLException e) {
        throw new RuntimeException(e);
      }
    }
    return wd;
  }

  /**
   * reset the webdriver, start new instance
   */
  public void resetWebDriver() {
    try {
      if (this.wd != null) {
        this.wd.close();
      }
      this.wd = initWebDriver();
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * init the webdriver
   * 
   * @return instance of WebDriver
   * @throws MalformedURLException
   */
  private WebDriver initWebDriver() throws MalformedURLException {
    WebDriver wd;
    // check if test should run on hub or locally
    if (seleniumProperties.getProperty("seleniumhub.usage") != null
        && Boolean.parseBoolean(seleniumProperties.getProperty("seleniumhub.usage"))) {
      wd = new RemoteWebDriver(new URL(seleniumProperties.getProperty("seleniumhub.url")),
          DesiredCapabilities.chrome());
    } else {
      // execute on chrome
      ChromeOptions options = prepareBrowserOptions(seleniumProperties);
      wd = new ChromeDriver(options);
    }
    // use implicit maximum wait for existence of elements
    wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    wd.manage().window().maximize();
    return wd;
  }

  /**
   * load properties from src/main/resources to configure selenium and testsuite
   * 
   * @throws FileNotFoundException
   * @throws IOException
   */
  private static Properties loadProperties() throws FileNotFoundException, IOException {
    Properties props = new Properties();
    props.load(new FileInputStream("src/main/resources/selenium.properties"));
    return props;
  }

  /**
   * prepare the Browser Options to control the browser
   * 
   * @return ChromeOptions for ChromeBrowser
   */
  private static ChromeOptions prepareBrowserOptions(Properties props) {
    initializeDriverPath();

    ChromeOptions opt = new ChromeOptions();
    List<String> arguments = new ArrayList<>();
    // headless option
    if (props.get("execute.headless") != null
        && Boolean.parseBoolean(props.get("execute.headless").toString())) {
      arguments.add("headless");
    }
    // window size
    final String propertyName = "window.size";
    if (props.getProperty(propertyName) != null) {
      final String regex = "[1-9][0-9]+x[1-9][0-9]+";
      final Pattern pattern = Pattern.compile(regex);
      final Matcher matcher = pattern.matcher(props.getProperty(propertyName));
      if (matcher.matches()) {
        arguments.add("window-size=" + props.getProperty(propertyName));
      }
    }
    HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
    opt.setExperimentalOption("prefs", chromePrefs);
    opt.addArguments(arguments);

    return opt;
  }

  /**
   * set system property to define chrome driver path for given os
   */
  private static void initializeDriverPath() {
    String chromeDriverPath = "";
    if (isWindows()) {
      chromeDriverPath = "driver/chromedriver_win32_2_40.exe";
    } else if (isMac()) {
      chromeDriverPath = "driver/chromedriver_mac64_2_40";
    } else if (isUnix()) {
      chromeDriverPath = "driver/chromedriver_linux64_2_40";
    }
    System.setProperty("webdriver.chrome.driver", chromeDriverPath);
  }

  private static boolean isWindows() {
    return (OS.indexOf("win") >= 0);
  }

  private static boolean isMac() {
    return (OS.indexOf("mac") >= 0);
  }

  private static boolean isUnix() {
    return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
  }

  public Properties getSeleniumProperties() {
    return seleniumProperties;
  }
}
