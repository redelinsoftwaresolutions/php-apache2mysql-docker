package de.redelin;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {"src/test/resources/features"}, strict = true,
    glue = {"de.redelin.steps"},
    plugin = {"json:build/test-results/result.json"}
)
/**
 * test suite executes the feature steps sorted by features.<br/>
 * it will handle the driver Helper which is used to get the current WebDriver to execute the
 * application
 * 
 * @author sredelin
 *
 */
public class TestSuite {

  public static CucumberWebDriverHelper webDriverHelper;

  @BeforeClass
  public static void setUp() {
    webDriverHelper = new CucumberWebDriverHelper();
  }

  @AfterClass
  public static void close() {
    webDriverHelper.getWebDriver().close();
  }
}
