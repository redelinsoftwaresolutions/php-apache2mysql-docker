package de.redelin.steps;

import org.junit.Assert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import de.redelin.CucumberWebDriverHelper;
import de.redelin.TestSuite;


public class ShowInitialAccessSteps {
  CucumberWebDriverHelper driverHelper = TestSuite.webDriverHelper;

  /**
   * define constructor of steps to use cucumber dependency injection
   */
  public ShowInitialAccessSteps() {
  }

  /**
   * reset the browser cache is necessary for DIVA application, due to javascript caching the field
   * inputs etc.<br/>
   * 
   * @throws Exception
   */
  @Given("^browser is started$")
  public void browser_is_started() throws Exception {
    driverHelper.getWebDriver().manage().deleteAllCookies();
    Assert.assertNotNull(driverHelper.getWebDriver());
  }

  @Given("^web application is called$")
  public void diva_web_application_is_called() throws Exception {
    driverHelper.getWebDriver()
        .get(driverHelper.getSeleniumProperties().getProperty("ui.endpoint.url"));
  }

  @When("^the web application is shown$")
  public void the_web_application_is_shown() throws Exception {
    final String pageTitlePart =
        driverHelper.getSeleniumProperties().getProperty("ui.endpoint.pagetitle.part");
    Assert.assertTrue(String.format("Pagetitle does not contain '%s'", pageTitlePart),
        driverHelper.getWebDriver().getTitle().contains(pageTitlePart));
  }
}
