package de.redelin.steps;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.redelin.CucumberWebDriverHelper;
import de.redelin.TestSuite;

public class RegistrationSteps {
  private CucumberWebDriverHelper driverHelper = TestSuite.webDriverHelper;

  /**
   * define constructor of steps to use cucumber dependency injection
   * 
   */
  public RegistrationSteps() {}

  @When("^I use the value \"([^\"]*)\" for field with id \"([^\"]*)\"$")
  public void i_use_the_value_for_field(String value, String fieldId) throws Exception {
    WebElement usernameField = driverHelper.getWebDriver().findElement(By.id(fieldId));
    usernameField.sendKeys(value);
  }

  @When("^focus on button with id \"([^\"]*)\" followed by pressing ENTER button$")
  public void focus_on_button_with_id_followed_by_pressing_ENTER_button(String sumbitButtonId)
      throws Exception {
    WebElement button = driverHelper.getWebDriver().findElement(By.id(sumbitButtonId));
    button.sendKeys(Keys.ENTER);
  }

  @When("^I accept the checkbox \"([^\"]*)\"$")
  public void i_accept_the_checkbox(String fieldId) throws Exception {
    WebElement checkBox = driverHelper.getWebDriver().findElement(By.id(fieldId));
    checkBox.sendKeys(Keys.SPACE);
  }

  @Then("^I want to receive a message \"([^\"]*)\" in field with id \"([^\"]*)\"\\.$")
  public void i_want_to_receive_a_message_in_field_with_id(String expectedMessage, String fieldId)
      throws Exception {
    WebElement message = driverHelper.getWebDriver().findElement(By.id(fieldId));
    Assert.assertEquals("text for message not as expected", expectedMessage, message.getText());
  }

  @When("^I perform a valid dummy registration \"([1-9][0-9]*)\" times$")
  public void i_perform_a_valid_dummy_registration_times(int times) throws Exception {
    // loop the registration an wait after eachregistration for 2 seconds
    for (int i = 0; i < times; i++) {
      i_use_the_value_for_field("Sven", "form_firstname");
      i_use_the_value_for_field("Redelin", "form_lastname");
      i_use_the_value_for_field("sven@redelin-soft.de", "form_email");
      i_use_the_value_for_field("sven@redelin-soft.de", "form_email_repeat");
      i_accept_the_checkbox("form_chk_approval");
      Thread.sleep(500);
      focus_on_button_with_id_followed_by_pressing_ENTER_button("form_submit");
      i_want_to_receive_a_message_in_field_with_id("Data processed successfully", "resultmessage");
      Thread.sleep(1500);
    }
  }
}
