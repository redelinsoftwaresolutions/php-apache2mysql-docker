#language: en
Feature: Diva Application Login won't be possible 
	User is not able to login into diva application
  
Background: 
	Given browser is started 
	And  web application is called 
	And the web application is shown 
	
Scenario: valid registration 
	When I use the value "Sven" for field with id "form_firstname" 
	And I use the value "Redelin" for field with id "form_lastname" 
	And I use the value "sven@redelin-soft.de" for field with id "form_email" 
	And I use the value "sven@redelin-soft.de" for field with id "form_email_repeat"
	And I accept the checkbox "form_chk_approval"
	And focus on button with id "form_submit" followed by pressing ENTER button
	Then I want to receive a message "Data processed successfully" in field with id "resultmessage". 

Scenario: valid registration hundred times
	When I perform a valid dummy registration "100" times 
