<?php
class Post {
	const POST_FIELD_ACTION_NAME = "actionName";
	private $actions = array ();
	
	/**
	 * default constructor
	 */
	public function __construct() {
		$this->initActions ();
	}
	
	/**
	 * init actions by registering them
	 */
	private function initActions() {
		$this->actions [] = 'demoform';
	}
	
	/**
	 * perform action
	 *
	 * @param unknown $postFields        	
	 */
	public function perform($postFields) {

		// check if action has been registered
		$this->checkCalledAction ( $postFields );
		
		// secure fields
		$postParams = $this->secureFields ( $postFields );
		
		// call function
		require_once __DIR__.'/../functions/posthandler_functions.php';
		$functionName = "action_" . $postFields [Post::POST_FIELD_ACTION_NAME];
		$functionName ( $postFields );
	}
	
	/**
	 * check if actionName has been set in postFields and action has been registered
	 *
	 * @param unknown $postFields        	
	 * @throws Exception
	 */
	private function checkCalledAction($postFields) {
		
		// field actionName is necessary
		if (! array_key_exists ( Post::POST_FIELD_ACTION_NAME, $postFields )) {
			throw new Exception ( 'post field actionName missing' );
		}
		
		// check if action has been registered
		$actionRegistered = false;
		foreach ( $this->actions as $actionName ) {
			if ($actionName == $postFields [Post::POST_FIELD_ACTION_NAME]) {
				$actionRegistered = true;
			}
		}
		if (! $actionRegistered) {
			throw new Exception ( 'function (' . $postFields [Post::POST_FIELD_ACTION_NAME] . ') has not been registered' );
		}
	}
	
	/**
	 * secure fields by updating and validating fields against systematic hacks
	 *
	 * @param unknown $postFields        	
	 */
	private function secureFields($postFields) {
		$postParams = array ();
		foreach ( $postFields as $key => $value ) {
			$postFields [$key] = htmlspecialchars ( $value );
		}
		return $postParams;
	}
	
}