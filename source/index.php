<?php
	require_once 'functions/config.php';
	
	$result = '';
	if (isset ( $_POST ['form_submit'] )) {
		// use post handler to execute the post with its functionality
		require_once __DIR__ . '/functions/posthandler.php';
	
		global $postActionParams;
	
		if (isset ( $postActionParams ['errormessage'] )) {
			$result = '<div id="resultmessage" class="errormessage">' . $postActionParams ['errormessage'] . '</div>';
		} else {
			$result = '<div id="resultmessage" class="successmessage">Data processed successfully</div>';
			$postActionParams = array();
		}
	}
	
	// avoid warnings of unset variables
	if(!isset($postActionParams) || sizeof($postActionParams) == 0) {
		$postActionParams = array();
		$postActionParams['form_firstname'] = '';
		$postActionParams['form_lastname'] = '';
		$postActionParams['form_email'] = '';
		$postActionParams['form_email_repeat'] = '';
	}
?>
<html>
	<head>
		<title>Demo page to add data for registration</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta charset="utf-8" />
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/form.css">
	</head>
	<body>
		<main>
			<article class="col-12-lg centerText">
				<h1>Registration</h1>
				You can register here for demo application.<br/>
				<form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>" method="post">
					<?php echo $result;?>
					<input type="hidden" id="actionName" name="actionName" value="demoform" />
					<fieldset>
						<div class="col-6-lg col-12-sm">
							<label for="form_firstname" class="col-12-sm">Firstname<span style="color: #FF0000">*</span></label>
							<input id="form_firstname" name="form_firstname" type="text" class="col-12-sm" value ="<?php echo $postActionParams['form_firstname']; ?>" />
						</div>	
						<div class="col-6-lg col-12-sm">
							<label for="form_lastname" class="col-12-sm">Lastname<span style="color: #FF0000">*</span></label>
							<input id="form_lastname" name="form_lastname" type="text" class="col-12-sm" value="<?php echo $postActionParams['form_lastname']; ?>" />
						</div>	
					</fieldset>
					<fieldset>
						<label for="form_email" class="col col-12-lg">E-Mail<span style="color: #FF0000">*</span></label>
						<input id="form_email" name="form_email" type="text" class="col col-12-lg" value="<?php echo $postActionParams['form_email']; ?>" />
					</fieldset>
					<fieldset>
						<label for="form_email_repeat" class="col col-12-lg">E-Mail repetition<span style="color: #FF0000">*</span></label>
						<input id="form_email_repeat" name="form_email_repeat" type="text" class="col col-12-lg" value="<?php echo $postActionParams['form_email_repeat']; ?>" />
					</fieldset>
					<fieldset>
						<input id="form_chk_approval" name="form_chk_approval" type="checkbox" value="1" class="col col-1-lg"/>
						<label for="form_chk_approval" class="col col-9-lg" ><span style="color: #FF0000;">*</span> <span style="text-align: inherit;">I accept the usage of my data for buying anything you want to my costs ;)</span></label>
					</fieldset>
					<fieldset>
						<input type="submit" id="form_submit" name="form_submit" value="Submit" class="col col-12-lg" />
					</fieldset>
					<fieldset>
						<label>Fields marked with <span style="color: #FF0000">*</span> are mandatory.</label>
					</fieldset>
				</form>				
			</article>
		</main>
	</body>
</html>