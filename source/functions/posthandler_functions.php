<?php
/*
 * Define all functions for post handler
 */

/**
 * Action for demoForm
 */
function action_demoform($postFields) {
	global $mysqli, $postActionParams;
	$postActionParams = $postFields;
	
	// trim field entries
	foreach ($postActionParams as $key => $value) {
        $postActionParams[$key] = trim($value);	    
	}

	// validate post params and abort execution if necessary
	$errormessages = action_demoform_validation ( $postActionParams );
	if ($errormessages !== true) {
		$errormessage = "<b>the following errors have occurred</b>";
		foreach ( $errormessages as $errm ) {
			$errormessage .= '<br/>';
			$errormessage .= $errm;
		}
		$postActionParams ["errormessage"] = $errormessage;
		return;
	}
	
	// save demo data
	$mysqli->query("INSERT INTO customer (firstname, lastname, email, datetime_registered)
			values ('".$postActionParams['form_firstname']."',
			'".$postActionParams['form_lastname']."',
			'".$postActionParams['form_email']."',
			'".date("Y-m-d H:i:s")."')") or die($mysqli->error);
}

/**
 * validation for action form demoform will return true if valid, else the concrete error messages.
 *
 * @param unknown $postActionParams        	
 * @return boolean|array(string)
 */
function action_demoform_validation($postActionParams) {
	$minLengthOfString = 3;
	$errorMessages = array ();
	
	if (! filter_var ( $postActionParams ['form_email'], FILTER_VALIDATE_EMAIL )) {
		$errorMessages [] = "Email-Address is not valid";
	}
	if ($postActionParams ['form_email'] != $postActionParams ['form_email_repeat']) {
		$errorMessages [] = "Email-Address repeat is not equal to Email-Address";
	}
	if (strlen ( $postActionParams ['form_firstname'] ) < $minLengthOfString) {
		$errorMessages [] = "Firstname to short";
	}
	if (strlen ( $postActionParams ['form_lastname'] ) < $minLengthOfString) {
		$errorMessages [] = "Lastname to short";
	}
	if (! isset ( $postActionParams ['form_chk_approval'] )) {
		$errorMessages [] = "Please approve the privacy statement";
	}

	if (sizeof ( $errorMessages ) == 0) {
		return true;
	} else {
		return $errorMessages;
	}
}