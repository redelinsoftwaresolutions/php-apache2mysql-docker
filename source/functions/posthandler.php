<?php
require_once __DIR__.'/config.php';
require_once __DIR__.'/../classes/post.class.php';

/**
 * perform post actions for post forms
 */
$postActionParams = array();
$post = new Post();
$post->perform($_POST);
