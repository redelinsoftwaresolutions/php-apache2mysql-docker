### Apache including php7, mysql server and phpMyAdmin as independent application
This configuration will create a container including apache server including php7 with mysqli  
as well as a container including phpMyAdmin  
as well as a container including a mysql-database-server  

# startup
## initial startup with docker-compose
```docker-compose up -d``` will startup mysql-db, apache and phpmyadmin containers  
## enhance system with another frontend container instance
```docker-compose run -d -p 81:80 --name second_apache apache```will startup another frontend application instance with defined name "second_apache" and port mapping internal 80 to 81
## robot to send massdata to application
the subdirectory ```robot```contains a java application buildable using gradle.  
please check the given README.MD in subdirectory to start the test robot.  

## connection:
# mysql-db
host in apache and phpMyAdminContainer: mysql_db
port: 3306 (default port)

# phpMyAdmin
host: localhost
port: 85

# apache
host: localhost
port: 80

## example usage of database using php
```
<?php
ini_set ( 'display_errors', 1 );
echo "DB read test<br/>";

/* Erstellt Connect zu Datenbank her */
$mysqli= new mysqli("mysql_db","root","test");
$mysqli->select_db("my_database");
/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

echo "<u>Example selection result:</u><br/>";
if($result = $mysqli->query("select * from my_table", MYSQLI_USE_RESULT)){
  while($row = $result->fetch_array()){
    echo $row["my_column"].'<br/>';
  }
  $result->close();
}
$mysqli->close();
```